package com.teamwork.peerreview.services;

import com.teamwork.peerreview.models.Comment;
import com.teamwork.peerreview.repositories.contracts.CommentRepository;
import com.teamwork.peerreview.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public void create(Comment comment) {
        commentRepository.create(comment);
    }

    @Override
    public void delete(int id) {
        commentRepository.delete(id);
    }
}
