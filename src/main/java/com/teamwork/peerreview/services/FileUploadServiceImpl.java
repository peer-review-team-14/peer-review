package com.teamwork.peerreview.services;

import com.teamwork.peerreview.models.Attachment;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.services.contracts.AttachmentService;
import com.teamwork.peerreview.services.contracts.FileUploadService;
import com.teamwork.peerreview.services.contracts.UserService;
import com.teamwork.peerreview.services.contracts.WorkItemService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileUploadServiceImpl implements FileUploadService {

    private final UserService userService;
    private final WorkItemService workItemService;
    private final AttachmentService attachmentService;

    public FileUploadServiceImpl(UserService userService, WorkItemService workItemService, AttachmentService attachmentService) {
        this.userService = userService;
        this.workItemService = workItemService;
        this.attachmentService = attachmentService;
    }

    @Override
    public void savePhoto(MultipartFile multipartFile, String username) {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        String uploadDir = "src/main/resources/static/img/profilePictures";
        User user = userService.getByUsername(username);
        user.setPhoto(fileName);
        userService.update(user, user);
        try {
            saveFile(uploadDir, fileName, multipartFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void saveWorkItemFile(MultipartFile multipartFile, String username, String workItemName) {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        String uploadDir = "src/main/resources/static/workItems/" + username + "/" + workItemName;
        WorkItem workItem = workItemService.getByName(workItemName);
        Attachment attachment = new Attachment();
        attachment.setUrl(fileName);
        attachment.setWorkItem(workItem);
        attachmentService.create(attachment);
        workItem.getAttachments().add(attachment);
        workItemService.update(workItem);

        try {
            saveFile(uploadDir, fileName, multipartFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void saveFile(String uploadDir, String fileName, MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {
            throw new IOException("Could not save image file: " + fileName, ioe);
        }
    }

}
