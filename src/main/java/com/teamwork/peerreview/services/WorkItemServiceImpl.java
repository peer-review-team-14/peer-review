package com.teamwork.peerreview.services;

import com.teamwork.peerreview.exceptions.DuplicateEntityException;
import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.exceptions.UnauthorizedUserException;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.repositories.contracts.WorkItemRepository;
import com.teamwork.peerreview.services.contracts.AttachmentService;
import com.teamwork.peerreview.services.contracts.CommentService;
import com.teamwork.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class WorkItemServiceImpl implements WorkItemService {
    public static final String YOU_ARE_NOT_AUTHORIZED_FOR_THIS_OPERATION = "You are not authorized for this operation.";
    private final WorkItemRepository workItemRepository;
    private final AttachmentService attachmentService;
    private final CommentService commentService;

    @Autowired
    public WorkItemServiceImpl(WorkItemRepository workItemRepository, AttachmentService attachmentService, CommentService commentService) {
        this.workItemRepository = workItemRepository;
        this.attachmentService = attachmentService;
        this.commentService = commentService;
    }

    @Override
    public List<WorkItem> getAll() {
        return workItemRepository.getAll();
    }

    @Override
    public WorkItem getById(int id) {
        return workItemRepository.getById(id);
    }

    @Override
    public WorkItem getByName(String name) {
        return workItemRepository.getByTitle(name);
    }

    @Override
    public List<WorkItem> listByStatus(int id) {
        return workItemRepository.listByStatus(id);
    }

    @Override
    public List<WorkItem> listByUser(int id) {
        return workItemRepository.listByUser(id);
    }

    @Override
    public void create(WorkItem workItem) {
        isNameDuplicated(workItem);
        workItemRepository.create(workItem);
    }

    @Override
    public void update(WorkItem workItem) {
        isNameDuplicated(workItem);
        workItemRepository.update(workItem);
    }

    @Override
    public void delete(int id, User user) {
        isAuthorized(id, user);
        WorkItem workItem = workItemRepository.getById(id);
        workItem.getAttachments()
                .forEach(attachment -> attachmentService.delete(attachment.getId()));
        workItem.getComments()
                        .forEach(comment -> commentService.delete(comment.getId()));
        workItemRepository.delete(id);
    }

    @Override
    public List<WorkItem> filter(Optional<Integer> statusId,
                                 Optional<Integer> reviewerId,
                                 int creatorId,
                                 Optional<String> sort) {
        return workItemRepository.filter(statusId, reviewerId, creatorId, sort);
    }

    @Override
    public List<WorkItem> sortBy(Optional<String> sort) {
        return workItemRepository.sortBy(sort);
    }


    private void isNameDuplicated(WorkItem workItem) {
        boolean nameExists = true;
        try {
            WorkItem workItemExists = workItemRepository.getByTitle(workItem.getTitle());
            if (workItemExists.getId() == workItem.getId()) {
                nameExists = false;
            }
        } catch (EntityNotFoundException e) {
            nameExists = false;
        }
        if (nameExists) {
            throw new DuplicateEntityException("Work item", "title", workItem.getTitle());
        }
    }

    private void isAuthorized(int id, User user) {
        WorkItem workItemToDelete = workItemRepository.getById(id);
        if (workItemToDelete.getUserCreator().getId() != user.getId() && !user.isAdmin()) {
            throw new UnauthorizedUserException(YOU_ARE_NOT_AUTHORIZED_FOR_THIS_OPERATION);
        }
    }
}
