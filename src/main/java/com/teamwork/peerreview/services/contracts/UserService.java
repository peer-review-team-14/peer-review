package com.teamwork.peerreview.services.contracts;

import com.teamwork.peerreview.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    void create(User user);

    void update(User userToUpdate, User user);

    void delete(int id, User user);

    List<User> search(Optional<String> searchItems, int teamId);
}
