package com.teamwork.peerreview.services.contracts;

import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {
    void savePhoto(MultipartFile multipartFile, String username);

    void saveWorkItemFile(MultipartFile multipartFile, String username, String workItemName);
}
