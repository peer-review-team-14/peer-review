package com.teamwork.peerreview.services.contracts;

import com.teamwork.peerreview.models.DTOs.WorkItemDto;
import com.teamwork.peerreview.models.Team;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;

import java.util.List;
import java.util.Optional;

public interface WorkItemService {

    List<WorkItem> getAll();

    WorkItem getById(int id);

    WorkItem getByName(String name);

    List<WorkItem> listByStatus(int id);

    List<WorkItem> listByUser(int id);

    void create(WorkItem workItem);

    void update(WorkItem workItem);

    void delete(int id, User user);

    List<WorkItem> filter(Optional<Integer> statusId,
                          Optional<Integer> reviewerId,
                          int creatorId,
                          Optional<String> sort);

    List<WorkItem> sortBy(Optional<String> sort);

}
