package com.teamwork.peerreview.services.contracts;

import com.teamwork.peerreview.models.Team;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface TeamService {
    List<Team> getAll();

    Team getById(int id);

    Team getByName(String name);

    List<WorkItem> getAllWorkItems(int id);

    List<Team> getAllTeamWhereUserBelong(int userId);

    void create(Team team);

    void update(Team team, User user);

    void delete(int id, User user);

    void addMemberToTeam(User memberToAdd, Team team, User user);

    void deleteMemberFromTeam(User memberToAdd, Team team, User use);

    List<User> getAllTeamMember(int id);


    Map<String, Set<String>> getAllTeamsAndMembers(int ownerId);

}
