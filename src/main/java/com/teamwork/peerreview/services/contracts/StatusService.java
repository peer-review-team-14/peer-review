package com.teamwork.peerreview.services.contracts;

import com.teamwork.peerreview.models.Status;

import java.util.List;

public interface StatusService {
    List<Status> getAll();

    Status getByName(String name);

    Status getById(int id);
}
