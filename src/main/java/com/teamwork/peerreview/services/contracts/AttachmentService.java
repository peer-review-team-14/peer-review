package com.teamwork.peerreview.services.contracts;

import com.teamwork.peerreview.models.Attachment;
import com.teamwork.peerreview.models.User;

import java.util.List;

public interface AttachmentService {
    Attachment getById(int id);

    void create(Attachment attachment);

    void update(Attachment attachment);

    void delete(int id);

    List<Attachment> getByWorkItemId(int workItemId);
}
