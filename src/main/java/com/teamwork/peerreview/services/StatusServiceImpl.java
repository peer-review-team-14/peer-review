package com.teamwork.peerreview.services;

import com.teamwork.peerreview.models.Status;
import com.teamwork.peerreview.repositories.contracts.StatusRepository;
import com.teamwork.peerreview.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class StatusServiceImpl implements StatusService {
    private final StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<Status> getAll() {
        return statusRepository.getAll();
    }

    @Override
    public Status getByName(String name) {
        return statusRepository.getByName(name);
    }

    @Override
    public Status getById(int id) {
        return statusRepository.getById(id);
    }
}
