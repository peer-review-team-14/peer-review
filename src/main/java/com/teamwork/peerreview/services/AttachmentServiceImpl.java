package com.teamwork.peerreview.services;

import com.teamwork.peerreview.exceptions.UnauthorizedUserException;
import com.teamwork.peerreview.models.Attachment;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.repositories.contracts.AttachmentRepository;
import com.teamwork.peerreview.repositories.contracts.UserRepository;
import com.teamwork.peerreview.services.contracts.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    private final AttachmentRepository attachmentRepository;


    @Autowired
    public AttachmentServiceImpl(AttachmentRepository attachmentRepository) {
        this.attachmentRepository = attachmentRepository;

    }

    @Override
    public Attachment getById(int id) {
        return attachmentRepository.getById(id);
    }

    @Override
    public void create(Attachment attachment) {
        attachmentRepository.create(attachment);
    }

    @Override
    public void update(Attachment attachment) {
        attachmentRepository.update(attachment);
    }

    @Override
    public void delete(int id) {
        attachmentRepository.delete(id);
    }

    @Override
    public List<Attachment> getByWorkItemId(int workItemId) {
        return attachmentRepository.getByWorkItemId(workItemId);
    }


}
