package com.teamwork.peerreview.services;

import com.teamwork.peerreview.exceptions.DuplicateEntityException;
import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.exceptions.UnauthorizedUserException;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.repositories.contracts.UserRepository;
import com.teamwork.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    public static final String YOU_ARE_NOT_AUTHORIZED_FOR_THIS_OPERATION = "You are not authorized for this operation.";
    UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUserName(username);
    }

    @Override
    public void create(User user) {
        isUserDuplicated(user);
        isEmailDuplicated(user);
        isPhoneDuplicated(user);
        userRepository.create(user);
    }

    @Override
    public void update(User userToUpdate, User user) {
        isAuthorized(userToUpdate, user);
        isUserDuplicated(userToUpdate);
        isEmailDuplicated(userToUpdate);
        isPhoneDuplicated(userToUpdate);
        userRepository.update(userToUpdate);
    }

    @Override
    public void delete(int id, User user) {
        isAuthorized(userRepository.getById(id), user);
        userRepository.delete(id);
    }

    @Override
    public List<User> search(Optional<String> searchItems, int teamId) {
        return userRepository.search(searchItems, teamId);
    }

    private void isUserDuplicated(User user) {
        boolean usernameExist = true;
        try {
            User existingUser = userRepository.getByUserName(user.getUsername());
            if (existingUser.getId() == user.getId()) {
                usernameExist = false;
            }
        } catch (EntityNotFoundException e) {
            usernameExist = false;
        }
        if (usernameExist) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
    }

    private void isEmailDuplicated(User user) {
        boolean emailExist = true;
        try {
            User existingUser = userRepository.getByEmail(user.getEmail());
            if (existingUser.getId() == user.getId()) {
                emailExist = false;
            }
        } catch (EntityNotFoundException e) {
            emailExist = false;
        }
        if (emailExist) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
    }

    private void isPhoneDuplicated(User user) {
        boolean phoneExist = true;
        try {
            User existingUser = userRepository.getByPhone(user.getPhoneNumber());
            if (existingUser.getId() == user.getId()) {
                phoneExist = false;
            }
        } catch (EntityNotFoundException e) {
            phoneExist = false;
        }
        if (phoneExist) {
            throw new DuplicateEntityException("User", "phone", user.getPhoneNumber());
        }
    }

    private void isAuthorized(User userToUpdate, User user) {
        if (userToUpdate.getId() != user.getId() && !user.isAdmin()) {
            throw new UnauthorizedUserException(YOU_ARE_NOT_AUTHORIZED_FOR_THIS_OPERATION);
        }
    }
}
