package com.teamwork.peerreview.services;

import com.teamwork.peerreview.exceptions.DuplicateEntityException;
import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.exceptions.UnauthorizedUserException;
import com.teamwork.peerreview.models.Team;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.repositories.contracts.TeamRepository;
import com.teamwork.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TeamServiceImpl implements com.teamwork.peerreview.services.contracts.TeamService {
    public static final String YOU_ARE_NOT_AUTHORIZED = "You are not authorized for this operation.";
    private final TeamRepository teamRepository;
    private final UserService userService;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository, UserService userService) {
        this.teamRepository = teamRepository;
        this.userService = userService;
    }

    @Override
    public List<Team> getAll() {
        return teamRepository.getAll();
    }

    @Override
    public Team getById(int id) {
        return teamRepository.getById(id);
    }

    @Override
    public Team getByName(String name) {
        return teamRepository.getByName(name);
    }

    @Override
    public List<WorkItem> getAllWorkItems(int id) {
        return new ArrayList<>(teamRepository.getById(id).getWorkItems());
    }

    @Override
    public List<Team> getAllTeamWhereUserBelong(int userId) {
        return teamRepository.getAllTeamWhereUserBelong(userId);
    }

    @Override
    public void create(Team team) {
        isTeamDuplicated(team);
        team.getUsers().add(team.getTeamOwner());
        teamRepository.create(team);
    }

    @Override
    public void update(Team team, User user) {
        isAuthenticated(team.getId(), user);
        isTeamDuplicated(team);
        teamRepository.update(team);

    }

    @Override
    public void delete(int id, User user) {
        isAuthenticated(id, user);
        teamRepository.delete(id);

    }

    @Override
    public void addMemberToTeam(User memberToAdd, Team team, User user) {
        if (team.getUsers().contains(memberToAdd)) {
            throw new DuplicateEntityException("Member in this team", "name", memberToAdd.getUsername());
        }
        team.getUsers().add(memberToAdd);
        update(team, user);

    }

    @Override
    public void deleteMemberFromTeam(User memberToAdd, Team team, User use) {
        if (team.getUsers().contains(memberToAdd)) {
            throw new EntityNotFoundException("Member", "name", team.getName());
        }
        team.getUsers().remove(memberToAdd);
        teamRepository.deleteMemberFromTeam(memberToAdd.getId());
    }

    @Override
    public List<User> getAllTeamMember(int id) {
        return new ArrayList<>(getById(id).getUsers());
    }


    private void isTeamDuplicated(Team team) {
        boolean teamExist = true;
        try {
            Team existingTeam = teamRepository.getByName(team.getName());

            if (existingTeam.getId() == team.getId()) {
                teamExist = false;
            }
        } catch (EntityNotFoundException e) {
            teamExist = false;
        }

        if (teamExist) {
            throw new DuplicateEntityException("Team", "name", team.getName());
        }
    }

    private void isAuthenticated(int id, User user) {
        if (user.getId() != teamRepository.getById(id).getTeamOwner().getId() && !user.isAdmin()) {
            throw new UnauthorizedUserException(YOU_ARE_NOT_AUTHORIZED);
        }
    }

    @Override
    public Map<String, Set<String>> getAllTeamsAndMembers(int ownerId) {
        User workItemOwner = userService.getById(ownerId);
        List<Team> teams = new ArrayList<>(getAllTeamWhereUserBelong(ownerId));
        Map<String, Set<String>> teamsAndMembers = new HashMap<>();
        for (Team team : teams) {
            Set<String> setOfUsernames = new HashSet<>();
            for (User user : team.getUsers()) {
                setOfUsernames.add(user.getUsername());
            }
            setOfUsernames.remove(workItemOwner.getUsername());
            teamsAndMembers.put(team.getName(), setOfUsernames);
        }
        return teamsAndMembers;

    }


//    public Map<String, Set<String>> getAllBrandsAndModelsInMap() {
//        List<CarBrand> brands = new ArrayList<>(carBrandRepository.getAll());
//        Map<String, Set<String>> brandsAndModels = new TreeMap<>();
//        for (CarBrand brand : brands) {
//            brandsAndModels.put(brand.getBrand(), carModelRepository.getAllModelsByBrand(brand.getId()));
//        }
//        return brandsAndModels;
//    }


}
