package com.teamwork.peerreview.controllers.modelMappers;

import com.teamwork.peerreview.models.Comment;
import com.teamwork.peerreview.models.DTOs.CreateWorkItemDto;
import com.teamwork.peerreview.models.DTOs.WorkItemDto;
import com.teamwork.peerreview.models.DTOs.WorkItemEditDto;
import com.teamwork.peerreview.models.DTOs.WorkItemUpdateDto;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WorkItemModelMapper {
    private final WorkItemService workItemService;
    private final StatusService statusService;
    private final UserService userService;
    private final TeamService teamService;
    private final CommentService commentService;

    @Autowired
    public WorkItemModelMapper(WorkItemService workItemService,
                               StatusService statusService,
                               UserService userService, TeamService teamService, CommentService commentService) {
        this.workItemService = workItemService;
        this.statusService = statusService;
        this.userService = userService;
        this.teamService = teamService;
        this.commentService = commentService;
    }

    public WorkItem fromDtoToWorkItem(WorkItemDto workItemDto) {
        WorkItem workItem = new WorkItem();
        dtoToObject(workItem, workItemDto);
        return workItem;

    }

    public WorkItem fromDtoToWorkItem(WorkItemDto workItemDto, int id) {
        WorkItem workItem = workItemService.getById(id);
        dtoToObject(workItem, workItemDto);
        return workItem;

    }

    public WorkItemDto fromWorkItemToDto(WorkItem workItem) {
        WorkItemDto workItemDto = new WorkItemDto();
        objectToDto(workItem, workItemDto);
        return workItemDto;
    }

    public WorkItemUpdateDto fromWorkItemToUpdateDto(WorkItem workItem) {
        WorkItemUpdateDto workItemUpdateDto = new WorkItemUpdateDto();
        objectToDto(workItem, workItemUpdateDto);
        return workItemUpdateDto;
    }

    public WorkItem fromUpdateDtoToWorkItem(WorkItemUpdateDto workItemUpdateDto, int id) {
        WorkItem workItem = workItemService.getById(id);
        dtoToObject(workItem, workItemUpdateDto);
        return workItem;
    }

    public WorkItemEditDto formWorkItemToEditDto(WorkItem workItem) {
        WorkItemEditDto workItemEditDto = new WorkItemEditDto();
        objectToDto(workItem, workItemEditDto);
        return workItemEditDto;
    }

    public WorkItem fromEditDtoToWorkItem(WorkItemEditDto workItemEditDto, int id) {
        WorkItem workItem = workItemService.getById(id);
        dtoToObject(workItem, workItemEditDto);
        return workItem;
    }


    public CreateWorkItemDto fromWorkItemToCreateWorkItemDto(WorkItem workItem) {
        CreateWorkItemDto createWorkItemDto = new CreateWorkItemDto();
        objectToDto(workItem, createWorkItemDto);
        return createWorkItemDto;

    }


    public WorkItem fromCreateWorkItemDtoToWorkItem(CreateWorkItemDto createWorkItemDto) {
        WorkItem workItem = new WorkItem();
        dtoToObject(workItem, createWorkItemDto);
        return workItem;

    }


    private void dtoToObject(WorkItem workItem, WorkItemDto workItemDto) {
        workItem.setTitle(workItemDto.getTitle());
        workItem.setDescription(workItemDto.getDescription());
        workItem.setComments(workItemDto.getComments());
        workItem.setStatus(statusService.getById(workItemDto.getStatusId()));
        workItem.setUserCreator(userService.getById(workItemDto.getUserCreatorId()));
        workItem.setReviewer(userService.getById(workItemDto.getReviewerId()));
    }

    private void objectToDto(WorkItem workItem, WorkItemDto workItemDto) {
        workItemDto.setTitle(workItem.getTitle());
        workItemDto.setDescription(workItem.getDescription());
        workItemDto.setComments(workItem.getComments());
        workItemDto.setStatusId(workItem.getStatus().getId());
        workItemDto.setUserCreatorId(workItem.getUserCreator().getId());
        workItemDto.setReviewerId(workItem.getReviewer().getId());
    }

    private void objectToDto(WorkItem workItem, CreateWorkItemDto createWorkItemDto) {
        createWorkItemDto.setTitle(workItem.getTitle());
        createWorkItemDto.setDescription(workItem.getDescription());
        createWorkItemDto.setComments(workItem.getComments());
        createWorkItemDto.setCreatorUsername(workItem.getUserCreator().getUsername());
        createWorkItemDto.setReviewerUsername(workItem.getReviewer().getUsername());
    }

    private void dtoToObject(WorkItem workItem, CreateWorkItemDto createWorkItemDto) {
        workItem.setTitle(createWorkItemDto.getTitle());
        workItem.setDescription(createWorkItemDto.getDescription());
        workItem.setComments(createWorkItemDto.getComments());
        workItem.setTeam(teamService.getByName(createWorkItemDto.getTeamName()));
        workItem.setUserCreator(userService.getByUsername(createWorkItemDto.getCreatorUsername()));
        workItem.setReviewer(userService.getByUsername(createWorkItemDto.getReviewerUsername()));
        workItem.setStatus(statusService.getById(createWorkItemDto.getStatusId()));
    }


    private void objectToDto(WorkItem workItem, WorkItemUpdateDto workItemUpdateDto) {
        workItemUpdateDto.setTitle(workItem.getTitle());
        workItemUpdateDto.setDescription(workItem.getDescription());
        workItemUpdateDto.setStatusId(workItem.getStatus().getId());
        workItemUpdateDto.setUserCreatorId(workItem.getUserCreator().getId());
        workItemUpdateDto.setReviewerId(workItem.getReviewer().getId());
    }

    private void objectToDto(WorkItem workItem, WorkItemEditDto workItemEditDto) {
        workItemEditDto.setTitle(workItem.getTitle());
        workItemEditDto.setDescription(workItem.getDescription());
    }

    private void dtoToObject(WorkItem workItem, WorkItemEditDto workItemEditDto) {

        workItem.setTitle(workItemEditDto.getTitle());
        workItem.setDescription(workItemEditDto.getDescription());


    }


    private void dtoToObject(WorkItem workItem, WorkItemUpdateDto workItemUpdateDto) {
//        workItem.setTitle(workItemUpdateDto.getTitle());
//        workItem.setDescription(workItemUpdateDto.getDescription());
//        workItem.setStatus(statusService.getById(workItemUpdateDto.getStatusId()));
//        workItem.setReviewer(userService.getById(workItemUpdateDto.getReviewerId()));
//        workItem.setUserCreator(userService.getById(workItemUpdateDto.getUserCreatorId()));
        Comment comment = new Comment();
        comment.setComment(workItemUpdateDto.getComments());
        comment.setWorkItem(workItem);
        comment.setCreator(workItem.getReviewer());
        commentService.create(comment);
        workItem.getComments().add(comment);

    }


}
