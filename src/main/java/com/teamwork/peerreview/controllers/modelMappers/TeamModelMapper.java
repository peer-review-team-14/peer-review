package com.teamwork.peerreview.controllers.modelMappers;

import com.teamwork.peerreview.models.DTOs.CreateTeamDto;
import com.teamwork.peerreview.models.DTOs.ShowTeamDto;
import com.teamwork.peerreview.models.Team;

import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.services.contracts.TeamService;
import com.teamwork.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class TeamModelMapper {
    private final TeamService teamService;
    private final UserService userService;
    private final UserModelMapper userModelMapper;

    @Autowired
    public TeamModelMapper(TeamService teamService, UserService userService, UserModelMapper userModelMapper) {
        this.teamService = teamService;
        this.userService = userService;
        this.userModelMapper = userModelMapper;
    }


    public Team fromDtoToTeam(CreateTeamDto createTeamDto){
        Team team = new Team();
        dtoToObject(team, createTeamDto);
        return team;
    }

    public Team fromDtoToTeam(CreateTeamDto createTeamDto, int id){
        Team team = teamService.getById(id);
        dtoToObject(team, createTeamDto);
        return team;
    }


    public ShowTeamDto fromTeamToDto(Team team){
        ShowTeamDto showTeamDto = new ShowTeamDto();
        objectToDto(team, showTeamDto);
        return showTeamDto;
    }

    private void objectToDto(Team team, ShowTeamDto showTeamDto){
        showTeamDto.setName(team.getName());
        showTeamDto.setShowTeamOwnerDto(userModelMapper.fromUserToDto(team.getTeamOwner()));
    }

    private void dtoToObject(Team team, CreateTeamDto createTeamDto){
        Set<User> members = new HashSet<>();
        for (Integer teamMember : createTeamDto.getTeamMembers()) {
            members.add(userService.getById(teamMember));
        }
        team.setName(createTeamDto.getName());
        team.setTeamOwner(userService.getById(createTeamDto.getTeamOwnerId()));
        team.setUsers(members);
    }
}
