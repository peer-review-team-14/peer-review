package com.teamwork.peerreview.controllers.modelMappers;

import com.teamwork.peerreview.models.DTOs.CreateUserDto;
import com.teamwork.peerreview.models.DTOs.EditUserDto;
import com.teamwork.peerreview.models.DTOs.EditUserPasswordDto;
import com.teamwork.peerreview.models.DTOs.ShowUserDto;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserModelMapper {
    private final UserService userService;

    @Autowired
    public UserModelMapper(UserService userService) {
        this.userService = userService;
    }

    public ShowUserDto fromUserToDto(User user) {
        ShowUserDto showUserDto = new ShowUserDto();
        objectToDto(user, showUserDto);
        return showUserDto;
    }

    public EditUserDto fromUserToEditUserDto(User user) {
        EditUserDto editUserDto = new EditUserDto();
        objectToDto(user, editUserDto);
        return editUserDto;
    }

    public User fromEditUserPasswordDtoToUser(EditUserPasswordDto editUserPasswordDto, int id) {
        User user = userService.getById(id);
        objectToDto(user, editUserPasswordDto);
        return user;
    }

    public User fromDtoToUser(CreateUserDto createUserDto) {
        User user = new User();
        dtoToObject(user, createUserDto);
        return user;

    }

    public User fromDtoToUser(CreateUserDto createUserDto, int id) {
        User user = userService.getById(id);
        dtoToObject(user, createUserDto);
        return user;
    }

    public User fromDtoToUser(EditUserDto editUserDto, int id) {
        User user = userService.getById(id);
        dtoToObject(user, editUserDto);
        return user;
    }

    private void dtoToObject(User user, CreateUserDto createUserDto) {
        user.setUsername(createUserDto.getUsername());
        user.setPassword(createUserDto.getPassword());
        user.setPhoneNumber(createUserDto.getPhoneNumber());
        user.setEmail(createUserDto.getEmail());
        user.setPhoto(createUserDto.getPhoto());
    }

    private void dtoToObject(User user, EditUserDto editUserDto) {
        user.setUsername(editUserDto.getUsername());
        user.setPhoneNumber(editUserDto.getPhoneNumber());
        user.setEmail(editUserDto.getEmail());
        user.setPhoto(editUserDto.getPhoto());
    }

    private void objectToDto(User user, ShowUserDto showUserDto) {
        showUserDto.setUsername(user.getUsername());
        showUserDto.setPhoneNumber(user.getPhoneNumber());
        showUserDto.setEmail(user.getEmail());
        showUserDto.setPhoto(user.getPhoto());
    }

    private void objectToDto(User user, EditUserDto editUserDto) {
        editUserDto.setUsername(user.getUsername());
        editUserDto.setPhoneNumber(user.getPhoneNumber());
        editUserDto.setEmail(user.getEmail());
        editUserDto.setPhoto(user.getPhoto());
    }

    private void objectToDto(User user, EditUserPasswordDto editUserPasswordDto) {
        user.setPassword(editUserPasswordDto.getPassword());
    }
}
