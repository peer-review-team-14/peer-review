package com.teamwork.peerreview.controllers.modelMappers;

import com.teamwork.peerreview.models.Comment;
import com.teamwork.peerreview.models.DTOs.CommentDto;
import com.teamwork.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentModelMapper {

    @Autowired
    public CommentModelMapper() {
    }

    public CommentDto objectToCommentDto(Comment comment) {
        CommentDto commentDto = new CommentDto();
        objectToDto(comment, commentDto);
        return commentDto;
    }


    private void objectToDto(Comment comment, CommentDto commentDto) {
        commentDto.setComment(comment.getComment());
        commentDto.setCreatorUserName(comment.getCreator().getUsername());
        commentDto.setWorkItemId(comment.getWorkItem().getId());

    }
}
