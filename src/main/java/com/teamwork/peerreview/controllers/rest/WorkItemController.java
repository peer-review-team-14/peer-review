package com.teamwork.peerreview.controllers.rest;

import com.teamwork.peerreview.controllers.AuthenticationHelper;
import com.teamwork.peerreview.controllers.modelMappers.CommentModelMapper;
import com.teamwork.peerreview.controllers.modelMappers.WorkItemModelMapper;
import com.teamwork.peerreview.exceptions.DuplicateEntityException;
import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.models.Comment;
import com.teamwork.peerreview.models.DTOs.CommentDto;
import com.teamwork.peerreview.models.DTOs.WorkItemDto;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/workItems")
public class WorkItemController {
    private final WorkItemService workItemService;
    private final AuthenticationHelper authenticationHelper;
    private final WorkItemModelMapper workItemModelMapper;
    private final CommentModelMapper commentModelMapper;

    @Autowired
    public WorkItemController(WorkItemService workItemService, AuthenticationHelper authenticationHelper, WorkItemModelMapper workItemModelMapper, CommentModelMapper commentModelMapper) {
        this.workItemService = workItemService;
        this.authenticationHelper = authenticationHelper;
        this.workItemModelMapper = workItemModelMapper;
        this.commentModelMapper = commentModelMapper;
    }

    @GetMapping
    public List<WorkItemDto> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
         return workItemService.getAll()
                 .stream()
                 .map(workItemModelMapper::fromWorkItemToDto)
                 .collect(Collectors.toList());
    }

    @GetMapping("/{id}/comments")
    public List<CommentDto> getComment(@PathVariable int id, @RequestHeader HttpHeaders headers){
        try {
            authenticationHelper.tryGetUser(headers);
            return workItemService.getById(id).getComments()
                    .stream()
                    .map(commentModelMapper::objectToCommentDto)
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }


    }

    @GetMapping("/{id}")
    public WorkItemDto getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return workItemModelMapper.fromWorkItemToDto(workItemService.getById(id));
    }

    @GetMapping("/listByStatus/{id}")
    public List<WorkItem> listByStatus(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return workItemService.listByStatus(id);
    }

    @GetMapping("/listByUsers/{id}")
    public List<WorkItem> listByUser(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return workItemService.listByUser(id);
    }

    @GetMapping("/filter")
    public List<WorkItem> filter(@RequestHeader HttpHeaders headers,
                          Optional<Integer> statusId,
                          Optional<Integer> reviewerId,
                          Optional<String> sort) {
        User user = authenticationHelper.tryGetUser(headers);
        return workItemService.filter( statusId, reviewerId, user.getId(), sort);
    }

    @GetMapping("/sortBy")
    public List<WorkItem> sortBy(Optional<String> sort, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return workItemService.sortBy(sort);
    }

    @PostMapping
    public WorkItemDto createWorkItem(@Valid @RequestBody WorkItemDto workItemDto,
                                      @RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            WorkItem workItem = workItemModelMapper.fromDtoToWorkItem(workItemDto);
            workItemService.create(workItem);
            return workItemDto;

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    public WorkItemDto update(@PathVariable int id,
                              @Valid @RequestBody WorkItemDto workItemDto,
                              @RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            WorkItem workItem = workItemModelMapper.fromDtoToWorkItem(workItemDto, id);
            workItemService.update(workItem);
            return workItemDto;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            workItemService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


}
