package com.teamwork.peerreview.controllers.rest;

import com.teamwork.peerreview.controllers.AuthenticationHelper;
import com.teamwork.peerreview.controllers.modelMappers.TeamModelMapper;
import com.teamwork.peerreview.controllers.modelMappers.UserModelMapper;
import com.teamwork.peerreview.exceptions.DuplicateEntityException;
import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.models.DTOs.CreateTeamDto;
import com.teamwork.peerreview.models.DTOs.ShowTeamDto;
import com.teamwork.peerreview.models.DTOs.ShowUserDto;
import com.teamwork.peerreview.models.Team;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.services.contracts.TeamService;
import com.teamwork.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/teams")
public class TeamController {
    private final TeamService teamService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final TeamModelMapper teamModelMapper;
    private final UserModelMapper userModelMapper;


    @Autowired
    public TeamController(TeamService teamService, UserService userService,
                          AuthenticationHelper authenticationHelper,
                          TeamModelMapper teamModelMapper,
                          UserModelMapper userModelMapper) {
        this.teamService = teamService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.teamModelMapper = teamModelMapper;
        this.userModelMapper = userModelMapper;
    }


    @GetMapping
    public List<ShowTeamDto> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return teamService.getAll().stream().map(teamModelMapper::fromTeamToDto).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Team getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return teamService.getById(id);
    }

    @PostMapping
    public CreateTeamDto createTeam(@Valid @RequestBody CreateTeamDto createTeamDto, @RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            Team team = teamModelMapper.fromDtoToTeam(createTeamDto);
            teamService.create(team);
            return createTeamDto;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/{teamId}/members")
    public ShowUserDto addMemberToTeam(@PathVariable int teamId, @RequestHeader HttpHeaders headers,
                                       @RequestParam(required = true) int memberId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            User memberToAdd = userService.getById(memberId);
            Team team = teamService.getById(teamId);
            teamService.addMemberToTeam(memberToAdd, team, user);
            return userModelMapper.fromUserToDto(memberToAdd);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}/workItems")
    public List<WorkItem> getAllWorkItems(@PathVariable int id) {
        return teamService.getAllWorkItems(id);
    }

    @GetMapping("/{teamId}/members")
    public List<ShowUserDto> showTeamMembers(@PathVariable int teamId,
                                             @RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
            return teamService.getAllTeamMember(teamId)
                    .stream()
                    .map(userModelMapper::fromUserToDto)
                    .collect(Collectors.toList());

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PutMapping("/{id}")
    public CreateTeamDto update(@PathVariable int id,
                                @Valid @RequestBody CreateTeamDto createTeamDto,
                                @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Team team = teamModelMapper.fromDtoToTeam(createTeamDto, id);
            teamService.update(team, user);
            return createTeamDto;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            teamService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{teamId}/members")
    public void deleteMemberFromTeam(@PathVariable int teamId, @RequestHeader HttpHeaders headers,
                                     @RequestParam(required = true) int memberId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            User memberToDelete = userService.getById(memberId);
            Team team = teamService.getById(teamId);
            teamService.deleteMemberFromTeam(memberToDelete, team, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

}
