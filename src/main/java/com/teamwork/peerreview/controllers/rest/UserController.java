package com.teamwork.peerreview.controllers.rest;

import com.teamwork.peerreview.controllers.AuthenticationHelper;
import com.teamwork.peerreview.controllers.modelMappers.UserModelMapper;
import com.teamwork.peerreview.exceptions.DuplicateEntityException;
import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.models.DTOs.CreateUserDto;
import com.teamwork.peerreview.models.DTOs.ShowUserDto;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;

    @Autowired
    public UserController(UserService userService,
                          AuthenticationHelper authenticationHelper,
                          UserModelMapper userModelMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
    }

    @GetMapping
    public List<ShowUserDto> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getAll().stream()
                .map(userModelMapper::fromUserToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getById(id);
    }

    @PostMapping
    public CreateUserDto createUser(@Valid @RequestBody CreateUserDto createUserDto) {
        try {
            User userToCreate = userModelMapper.fromDtoToUser(createUserDto);
            userService.create(userToCreate);
            return createUserDto;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public CreateUserDto update(@PathVariable int id,
                                @Valid @RequestBody CreateUserDto createUserDto,
                                @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            User userToUpdate = userModelMapper.fromDtoToUser(createUserDto, id);
            userService.update(userToUpdate, user);
            return createUserDto;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id,
                       @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

//    @GetMapping("/search")
//    public List<User> search(@RequestParam(required = false) Optional<String> search,
//                             @RequestHeader HttpHeaders headers) {
//        User user = authenticationHelper.tryGetUser(headers);
//        return userService.search(search);
//    }
}
