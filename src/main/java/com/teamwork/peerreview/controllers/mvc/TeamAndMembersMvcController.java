package com.teamwork.peerreview.controllers.mvc;

import com.teamwork.peerreview.controllers.AuthenticationHelper;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.services.contracts.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.Set;

@Controller
public class TeamAndMembersMvcController {
    private final TeamService teamService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public TeamAndMembersMvcController(TeamService teamService, AuthenticationHelper authenticationHelper) {
        this.teamService = teamService;
        this.authenticationHelper = authenticationHelper;
    }

    @RequestMapping("/members")
    @ResponseBody
    public Set<String> getMembers(@RequestParam String teamName, HttpSession session) {
        User workItemOwner = authenticationHelper.tryGetUser(session);
        Map<String, Set<String>> teamsAndMembers = teamService.getAllTeamsAndMembers(workItemOwner.getId());
        return teamsAndMembers.get(teamName);

    }


}
