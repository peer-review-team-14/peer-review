package com.teamwork.peerreview.controllers.mvc;

import com.teamwork.peerreview.controllers.modelMappers.UserModelMapper;
import com.teamwork.peerreview.exceptions.DuplicateEntityException;
import com.teamwork.peerreview.models.DTOs.CreateUserDto;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/register")
public class RegisterMvcController {
    private final UserService userService;
    private final UserModelMapper userModelMapper;

    @Autowired
    public RegisterMvcController(UserService userService, UserModelMapper userModelMapper) {
        this.userService = userService;
        this.userModelMapper = userModelMapper;
    }

    @GetMapping()
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new CreateUserDto());
        return "register";
    }

    @PostMapping()
    public String handleRegister(@Valid @ModelAttribute("register") CreateUserDto createUserDto,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        if(!createUserDto.getPassword().equals(createUserDto.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error",
                    "Password confirmation should match password");
            return "register";
        }
        try {
            User userToCreate = userModelMapper.fromDtoToUser(createUserDto);
            userService.create(userToCreate);
            return "redirect:/login";
        }catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        }
    }
}
