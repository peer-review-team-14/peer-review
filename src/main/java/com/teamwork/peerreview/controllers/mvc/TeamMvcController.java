package com.teamwork.peerreview.controllers.mvc;

import com.teamwork.peerreview.controllers.AuthenticationHelper;
import com.teamwork.peerreview.controllers.modelMappers.TeamModelMapper;
import com.teamwork.peerreview.controllers.modelMappers.UserModelMapper;
import com.teamwork.peerreview.exceptions.AuthenticationFailureException;
import com.teamwork.peerreview.exceptions.DuplicateEntityException;
import com.teamwork.peerreview.exceptions.UnauthorizedUserException;
import com.teamwork.peerreview.models.DTOs.CreateTeamDto;
import com.teamwork.peerreview.models.DTOs.FilterWorkItemDto;
import com.teamwork.peerreview.models.DTOs.SearchUserDto;
import com.teamwork.peerreview.models.Status;
import com.teamwork.peerreview.models.Team;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.services.contracts.StatusService;
import com.teamwork.peerreview.services.contracts.TeamService;
import com.teamwork.peerreview.services.contracts.UserService;
import com.teamwork.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/teams")
public class TeamMvcController {
    private final TeamService teamService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;
    private final UserService userService;
    private final TeamModelMapper teamModelMapper;
    private final StatusService statusService;
    private final WorkItemService workItemService;

    @Autowired
    public TeamMvcController(TeamService teamService,
                             AuthenticationHelper authenticationHelper,
                             UserModelMapper userModelMapper,
                             UserService userService,
                             TeamModelMapper teamModelMapper,
                             StatusService statusService,
                             WorkItemService workItemService) {
        this.teamService = teamService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
        this.userService = userService;
        this.teamModelMapper = teamModelMapper;
        this.statusService = statusService;
        this.workItemService = workItemService;
    }

    @ModelAttribute("teams")
    public Object populateTeams(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return teamService.getAllTeamWhereUserBelong(authenticationHelper.tryGetUser(session).getId());
    }

    @ModelAttribute("user")
    public Object populateUser(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return userModelMapper.fromUserToEditUserDto(authenticationHelper.tryGetUser(session));
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses() {
        return statusService.getAll();
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of("Id, ascending", "Id, descending",
                "Title, ascending", "Title, descending",
                "Status, ascending", "Status, descending"));
    }

    @ModelAttribute("allUsers")
    public Object getAllUsers(HttpSession session) {
        User creator;
        try {
            creator = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        List<User> allUsers = userService.getAll();
        allUsers.remove(creator);
        return allUsers;
    }

    @GetMapping("/new")
    public String showCreateTeamsPage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("createTeamDto", new CreateTeamDto());
            return "team-new";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }

    @PostMapping("/new")
    public String handleCreateTeamsPage(@Valid @ModelAttribute("createTeamDto") CreateTeamDto createTeamDto,
                                        HttpSession session,
                                        BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                return "teams/new";
            }
            User creator = authenticationHelper.tryGetUser(session);
            createTeamDto.setTeamOwnerId(creator.getId());
            Team team = teamModelMapper.fromDtoToTeam(createTeamDto);
            teamService.create(team);
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("Team", "team_name_error", e.getMessage());
            return "redirect:/teams/new";
        }
    }

    @GetMapping("/{id}/workitems")
    public String showTeamWorkItems(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            List<WorkItem> workItems = new ArrayList<>(teamService.getAllWorkItems(id));
            model.addAttribute("workitems", workItems);
            model.addAttribute("filterWorkItemDto", new FilterWorkItemDto());
            Set<User> reviewers = new HashSet<>();
            teamService.getAllWorkItems(id)
                    .forEach(workItem -> reviewers.add(workItem.getReviewer()));
            model.addAttribute("reviewers", reviewers);
            return "workitems";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }

    @GetMapping("/{id}/members")
    public String showTeamMembers(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            Team team = teamService.getById(id);
            model.addAttribute("team", team);
            model.addAttribute("members", teamService.getAllTeamMember(id));
            model.addAttribute("searchUserDto", new SearchUserDto());
            return "team-members";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }

    @PostMapping("/{id}/members/search")
    public String search(@PathVariable int id, @ModelAttribute("searchUserDto") SearchUserDto searchUserDto,
                         Model model,
                         HttpSession session) {
        List<User> members = userService.search(Optional.of(searchUserDto.getSearch()), id);
        Team team = teamService.getById(id);
        model.addAttribute("team", team);
        model.addAttribute("members", members);
        model.addAttribute("searchUserDto", new SearchUserDto());
        return "team-members";
    }

    @GetMapping("/{id}/addMembers")
    public String showAddMembers(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            List<User> allUsers = userService.getAll();
            Team team = teamService.getById(id);
            team.getUsers().forEach(allUsers::remove);
            CreateTeamDto createTeamDto = new CreateTeamDto();
            createTeamDto.setName(team.getName());
            model.addAttribute("team", team);
            model.addAttribute("allMembers", allUsers);
            model.addAttribute("createTeamDto", createTeamDto);
            return "team-add-members";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }

    @PostMapping("/{id}/addMembers")
    public String handleAddMembers(@PathVariable int id, @ModelAttribute("createTeamDto") CreateTeamDto createTeamDto,
                                   HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Team team = teamService.getById(id);
            createTeamDto.getTeamMembers()
                    .forEach(memberId -> team.getUsers().add(userService.getById(memberId)));
            teamService.update(team, user);
            return "redirect:/teams/" + id + "/members";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }

    @GetMapping("/{id}/members/{memberId}/delete")
    public String removeMemberFromTeam(@PathVariable int id,
                                       @PathVariable int memberId,
                                       Model model,
                                       HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            Team team = teamService.getById(id);
            team.getUsers().remove(userService.getById(memberId));
            teamService.update(team, user);
            return "redirect:/teams/" + id + "/members";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        } catch (UnauthorizedUserException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/login";
        }
    }


}