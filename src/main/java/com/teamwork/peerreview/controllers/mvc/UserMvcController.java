package com.teamwork.peerreview.controllers.mvc;

import com.teamwork.peerreview.controllers.AuthenticationHelper;
import com.teamwork.peerreview.controllers.modelMappers.UserModelMapper;
import com.teamwork.peerreview.exceptions.AuthenticationFailureException;
import com.teamwork.peerreview.exceptions.DuplicateEntityException;
import com.teamwork.peerreview.models.DTOs.EditUserDto;
import com.teamwork.peerreview.models.DTOs.EditUserPasswordDto;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.services.contracts.FileUploadService;
import com.teamwork.peerreview.services.contracts.TeamService;
import com.teamwork.peerreview.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;
    private final TeamService teamService;
    private final FileUploadService fileUploadService;

    @Autowired
    public UserMvcController(UserService userService,
                             AuthenticationHelper authenticationHelper,
                             UserModelMapper userModelMapper,
                             TeamService teamService,
                             FileUploadService fileUploadService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
        this.teamService = teamService;
        this.fileUploadService = fileUploadService;
    }

    @ModelAttribute("teams")
    public Object populateTeams(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return teamService.getAllTeamWhereUserBelong(authenticationHelper.tryGetUser(session).getId());
    }

    @ModelAttribute("user")
    public Object populateUser(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return userModelMapper.fromUserToEditUserDto(authenticationHelper.tryGetUser(session));
    }

    @GetMapping
    public String showAllUsers(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        model.addAttribute("users", userService.getAll());
        return "users";
    }

    @GetMapping("/profile")
    public String showUserById(HttpSession session, Model model) {
        try {
            EditUserDto editUserDto = userModelMapper.fromUserToEditUserDto(authenticationHelper.tryGetUser(session));
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("user", editUserDto);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return "profile";
    }

    @PostMapping("/profile/{username}/update")
    public String handleProfileUpdate(@PathVariable String username,
                                      @Valid @ModelAttribute("user") EditUserDto editUserDto,
                                      HttpSession session,
                                      BindingResult bindingResult,
                                      Model model) {
        User updater;
        try {
            updater = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        if (bindingResult.hasErrors()) {
            return "profile";
        }
        try {
            User userToUpdate = userModelMapper.fromDtoToUser(editUserDto, userService.getByUsername(username).getId());
            userService.update(userToUpdate, updater);
            return "redirect:/users/profile";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "profile";
        }

    }

    @GetMapping("/profile/update/password")
    public String showProfileUpdatePassword(HttpSession session,
                                            Model model) {
        try {
            EditUserDto editUserDto = userModelMapper.fromUserToEditUserDto(authenticationHelper.tryGetUser(session));
            model.addAttribute("user", editUserDto);
            model.addAttribute("userPasswordEdit", new EditUserPasswordDto());
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return "change-password";

    }

    @PostMapping("/profile/{username}/update/password")
    public String handleProfileUpdatePassword(@PathVariable String username,
                                              @Valid @ModelAttribute("userPasswordEdit") EditUserPasswordDto editUserPasswordDto,
                                              HttpSession session,
                                              BindingResult bindingResult,
                                              Model model) {
        User updater;
        try {
            updater = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        if (bindingResult.hasFieldErrors()) {
            return "change-password";
        }
        if (!editUserPasswordDto.getPassword().equals(editUserPasswordDto.getPasswordConfirm())) {
            bindingResult.rejectValue("password", "password_error",
                    "Password confirmation should match password");
            return "change-password";
        }
        try {
            User userToUpdate = userService.getByUsername(username);
            userToUpdate.setPassword(editUserPasswordDto.getPassword());
            userService.update(userToUpdate, updater);
            return "redirect:/users/profile";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "change-password";
        }
    }

    @GetMapping("/profile/update/photo")
    public String showProfileUpdatePicture(HttpSession session,
                                            Model model) {
        try {
            EditUserDto editUserDto = userModelMapper.fromUserToEditUserDto(authenticationHelper.tryGetUser(session));
            authenticationHelper.tryGetUser(session);
            model.addAttribute("user", editUserDto);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return "change-profilePhoto";
    }

    @PostMapping("/{username}/upload/photos")
    public String uploadProfilePicture(@RequestParam("profilePhoto") MultipartFile multipartFile,
                                       @PathVariable String username) {
        fileUploadService.savePhoto(multipartFile, username);
        return "redirect:/users/profile";
    }

//    @PostMapping("/search")
//    public String search(@ModelAttribute("searchUserDto") SearchUserDto searchUserDto,
//                         Model model,
//                         HttpSession session) {
//        List<User> members = userService.search(Optional.of(searchUserDto.getSearch()));
//        model.addAttribute("members", members);
//        return "team-members";
//    }

}
