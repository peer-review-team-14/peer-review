package com.teamwork.peerreview.controllers.mvc;

import com.teamwork.peerreview.controllers.AuthenticationHelper;
import com.teamwork.peerreview.controllers.modelMappers.UserModelMapper;
import com.teamwork.peerreview.controllers.modelMappers.WorkItemModelMapper;
import com.teamwork.peerreview.exceptions.AuthenticationFailureException;
import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.exceptions.UnauthorizedUserException;
import com.teamwork.peerreview.models.DTOs.CreateWorkItemDto;
import com.teamwork.peerreview.models.DTOs.FilterWorkItemDto;
import com.teamwork.peerreview.models.DTOs.WorkItemEditDto;
import com.teamwork.peerreview.models.DTOs.WorkItemUpdateDto;
import com.teamwork.peerreview.models.Status;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.services.StatusServiceImpl;
import com.teamwork.peerreview.services.contracts.FileUploadService;
import com.teamwork.peerreview.services.contracts.TeamService;
import com.teamwork.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/workitems")
public class WorkItemMvcController {
    private final TeamService teamService;
    private final WorkItemService workItemService;
    private final StatusServiceImpl statusService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;
    private final WorkItemModelMapper workItemModelMapper;
    private final FileUploadService fileUploadService;

    @Autowired
    public WorkItemMvcController(TeamService teamService,
                                 WorkItemService workItemService,
                                 StatusServiceImpl statusService,
                                 AuthenticationHelper authenticationHelper,
                                 UserModelMapper userModelMapper, WorkItemModelMapper workItemModelMapper, FileUploadService fileUploadService) {
        this.teamService = teamService;
        this.workItemService = workItemService;
        this.statusService = statusService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
        this.workItemModelMapper = workItemModelMapper;
        this.fileUploadService = fileUploadService;
    }


    @ModelAttribute("teams")
    public Object populateTeams(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return teamService.getAllTeamWhereUserBelong(authenticationHelper.tryGetUser(session).getId());
    }

    @ModelAttribute("user")
    public Object populateUser(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return userModelMapper.fromUserToEditUserDto(authenticationHelper.tryGetUser(session));
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses() {
        return statusService.getAll();
    }


    @GetMapping
    public String showWorkItemsPage() {

        return "workitems";
    }

    @GetMapping("/new")
    public String showNewWorkItemPage(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("WorkItem", new CreateWorkItemDto());
            Map<String, Set<String>> teamUsers = teamService.getAllTeamsAndMembers(user.getId());
            model.addAttribute("allTeams", teamUsers);
            return "workitem-new";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }

    @PostMapping("/new")
    public String createWorkItem(@ModelAttribute("WorkItem") CreateWorkItemDto createWorkItemDto,
                                 HttpSession session,
                                 Model model,
                                 @RequestParam("workItemFile") MultipartFile multipartFile) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        createWorkItemDto.setCreatorUsername(user.getUsername());
        createWorkItemDto.setStatusId(3);
        WorkItem workItem = workItemModelMapper.fromCreateWorkItemDtoToWorkItem(createWorkItemDto);
        workItemService.create(workItem);
        fileUploadService.saveWorkItemFile(multipartFile, user.getUsername(), workItem.getTitle());
        return "redirect:/";


    }

//    @GetMapping("{id}/view")
//    public String showViewWorkItemPage(@PathVariable int id, HttpSession session, Model model) {
//        try {
//            User user = authenticationHelper.tryGetUser(session);
//            WorkItem workItem = workItemService.getById(id);
//            model.addAttribute("workItem", workItem);
//            model.addAttribute("attachments", workItem.getAttachments());
//            return "workitem-view";
//        } catch (AuthenticationFailureException e) {
//            return "redirect:/login";
//        }
//    }


    @GetMapping("/{id}/view")
    public String showEditWorkItem(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            WorkItem workItem = workItemService.getById(id);
            WorkItemUpdateDto workItemUpdateDto = workItemModelMapper.fromWorkItemToUpdateDto(workItem);
            model.addAttribute("workItem", workItem);
            model.addAttribute("workItemDto", workItemUpdateDto);
            model.addAttribute("attachments", workItem.getAttachments());
            model.addAttribute("isReviewer", isReviewer(workItem, user));
            return "workitem-view";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }

    @PostMapping("/{id}/view")
    public String updateWorkItem(@PathVariable int id, @ModelAttribute("workItemDto") WorkItemUpdateDto workItemUpdateDto,
                                 BindingResult bindingResult,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }


        if (bindingResult.hasErrors()) {
            model.addAttribute("workItem", workItemService.getById(id));
            return "workitem-view";
        }

        try {
            WorkItem workitem = workItemModelMapper.fromUpdateDtoToWorkItem(workItemUpdateDto, id);
            workItemService.update(workitem);
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("{id}/update")
    public String showEditWorkitem(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            WorkItem workItem = workItemService.getById(id);
            WorkItemEditDto workItemEditDto = workItemModelMapper.formWorkItemToEditDto(workItem);
            model.addAttribute("workItem", workItem);
            model.addAttribute("attachments", workItem.getAttachments());
            model.addAttribute("workItemDto", workItemEditDto);

            return "workitem-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }


    }

    @PostMapping("{id}/update")
    public String editWorkItem(@PathVariable int id, @ModelAttribute("workItemDto") WorkItemEditDto workItemEditDto,
                               BindingResult bindingResult,
                               Model model,
                               HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }


        if (bindingResult.hasErrors()) {
            model.addAttribute("workItem", workItemService.getById(id));
            return "workitem-view";
        }
        try {
            WorkItem workitem = workItemModelMapper.fromEditDtoToWorkItem(workItemEditDto, id);
            workItemService.update(workitem);
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

    }

    @PostMapping()
    public String filter(@ModelAttribute("filterWorkItemDto") FilterWorkItemDto filterWorkItemDto,
                         Model model,
                         HttpSession session) {
        List<WorkItem> workItems = workItemService.filter(Optional.of(filterWorkItemDto.getStatusId()),
                Optional.of(filterWorkItemDto.getReviewerId()),
                authenticationHelper.tryGetUser(session).getId(),
                Optional.of(filterWorkItemDto.getSort()));

        model.addAttribute("workitems", workItems);
        return "workitems";
    }


    @GetMapping("/{id}/delete")
    public String deleteWorkItem(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            workItemService.delete(id, user);
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedUserException e) {
            model.addAttribute("error", e.getMessage());
            return "login";
        }


    }


    private boolean isReviewer(WorkItem workItem, User user) {
        return workItem.getReviewer().getId() == user.getId();
    }

    private boolean isCreator(WorkItem workItem, User user) {
        return workItem.getUserCreator().getId() == user.getId();
    }
}