package com.teamwork.peerreview.controllers.mvc;

import com.teamwork.peerreview.controllers.AuthenticationHelper;
import com.teamwork.peerreview.controllers.modelMappers.UserModelMapper;
import com.teamwork.peerreview.exceptions.AuthenticationFailureException;
import com.teamwork.peerreview.models.DTOs.FilterWorkItemDto;
import com.teamwork.peerreview.models.Status;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.services.contracts.StatusService;
import com.teamwork.peerreview.services.contracts.TeamService;
import com.teamwork.peerreview.services.contracts.WorkItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping("/")
public class IndexMvcController {
    private final TeamService teamService;
    private final AuthenticationHelper authenticationHelper;
    private final UserModelMapper userModelMapper;
    private final WorkItemService workItemService;
    private final StatusService statusService;

    @Autowired
    public IndexMvcController(TeamService teamService,
                              AuthenticationHelper authenticationHelper,
                              UserModelMapper userModelMapper,
                              WorkItemService workItemService,
                              StatusService statusService) {
        this.teamService = teamService;
        this.authenticationHelper = authenticationHelper;
        this.userModelMapper = userModelMapper;
        this.workItemService = workItemService;
        this.statusService = statusService;
    }

    @ModelAttribute("teams")
    public Object populateTeams(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return teamService.getAllTeamWhereUserBelong(authenticationHelper.tryGetUser(session).getId());
    }

    @ModelAttribute("user")
    public Object populateUser(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        return userModelMapper.fromUserToEditUserDto(authenticationHelper.tryGetUser(session));
    }

    @ModelAttribute("reviewers")
    public Object populateReviewers(HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        Set<User> reviewers = new HashSet<>();
        workItemService
                .listByUser(authenticationHelper.tryGetUser(session).getId())
                .forEach(workItem -> reviewers.add(workItem.getReviewer()));
        return reviewers;
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses() {
        return statusService.getAll();
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of("Id, ascending", "Id, descending",
                "Title, ascending", "Title, descending",
                "Status, ascending", "Status, descending"));
    }

    @GetMapping
    public String showIndexPage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        model.addAttribute("workitems",
                workItemService.listByUser(authenticationHelper.tryGetUser(session).getId()));
        model.addAttribute("filterWorkItemDto", new FilterWorkItemDto());
        return "index";
    }

    @PostMapping("filter")
    public String filter(@ModelAttribute("filterWorkItemDto") FilterWorkItemDto filterWorkItemDto,
                         Model model,
                         HttpSession session) {
        List<WorkItem> workItems = workItemService.filter(Optional.of(filterWorkItemDto.getStatusId()),
                Optional.of(filterWorkItemDto.getReviewerId()),
                authenticationHelper.tryGetUser(session).getId(),
                Optional.of(filterWorkItemDto.getSort()));

        model.addAttribute("workitems", workItems);
        return "index";
    }

}








