package com.teamwork.peerreview.repositories;

import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.models.Comment;
import com.teamwork.peerreview.models.Team;
import com.teamwork.peerreview.repositories.contracts.CommentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CommentRepositoryImpl implements CommentRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CommentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Comment comment) {

        try (Session session = sessionFactory.openSession()) {
            var transaction = session.beginTransaction();
            session.save(comment);
            transaction.commit();
        }

    }


    @Override
    public void delete(int id) {
        Comment commentToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            var transaction = session.beginTransaction();
            session.delete(commentToDelete);
            transaction.commit();
        }
    }


    @Override
    public Comment getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Comment comment = session.get(Comment.class, id);
            if (comment == null) {
                throw new EntityNotFoundException("Comment", id);
            }
            return comment;
        }
    }
}
