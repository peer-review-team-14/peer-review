package com.teamwork.peerreview.repositories;


import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from User ", User.class).list();
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User getByUserName(String username) {
        try (Session session = sessionFactory.openSession()) {
            List<User> result = session
                    .createQuery("from User where username = :username", User.class)
                    .setParameter("username", username)
                    .list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(0);
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            List<User> result = session
                    .createQuery("from User where email = :email", User.class)
                    .setParameter("email", email)
                    .list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return result.get(0);
        }
    }

    @Override
    public User getByPhone(String userPhoneNumber) {
        try (Session session = sessionFactory.openSession()) {
            List<User> result = session
                    .createQuery("from User where phoneNumber = :phoneNumber", User.class)
                    .setParameter("phoneNumber", userPhoneNumber)
                    .list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "phone", userPhoneNumber);
            }
            return result.get(0);
        }
    }

    @Override
    public List<User> search(Optional<String> search, int teamId) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder result = new StringBuilder("from User ");
            search.ifPresent(value ->
                    result.append("where username like '%").append(value)
                            .append("%' or email like '%").append(value)
                            .append("%' or phoneNumber like '%")
                            .append(value).append("%'"));
            System.out.println(result);
            return session.createQuery(result.toString(), User.class).list();
        }
    }



}