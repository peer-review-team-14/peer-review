package com.teamwork.peerreview.repositories.contracts;

import com.teamwork.peerreview.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getAll();

    User getByUserName(String username);

    void create(User user);

    void update(User user);

    void delete(int id);

    User getById(int id);

    User getByEmail(String email);

    User getByPhone(String phone);

    List<User> search(Optional<String> search, int teamId);

}
