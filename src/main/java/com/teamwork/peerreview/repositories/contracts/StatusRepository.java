package com.teamwork.peerreview.repositories.contracts;

import com.teamwork.peerreview.models.Status;

import java.util.List;

public interface StatusRepository {

    List<Status> getAll();

    Status getByName(String name);

    Status getById(int id);
}
