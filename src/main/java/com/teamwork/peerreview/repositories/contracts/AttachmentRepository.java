package com.teamwork.peerreview.repositories.contracts;

import com.teamwork.peerreview.models.Attachment;

import java.util.List;

public interface AttachmentRepository {
    Attachment getById(int id);

    void create(Attachment attachment);

    void update(Attachment attachment);

    void delete(int id);

    List<Attachment> getByWorkItemId(int workItemId);
}
