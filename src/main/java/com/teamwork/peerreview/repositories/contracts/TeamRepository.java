package com.teamwork.peerreview.repositories.contracts;

import com.teamwork.peerreview.models.Team;
import com.teamwork.peerreview.models.User;

import java.util.List;

public interface TeamRepository {

    List<Team> getAll();

    Team getById(int id);

    Team getByName(String name);

    List<Team> getAllTeamWhereUserBelong(int userId);

    void create(Team team);

    void update(Team team);

    void delete(int id);

    void deleteMemberFromTeam(int id);



}
