package com.teamwork.peerreview.repositories.contracts;

import com.teamwork.peerreview.models.WorkItem;

import java.util.List;
import java.util.Optional;

public interface WorkItemRepository {

    List<WorkItem> getAll();

    WorkItem getById(int id);

    WorkItem getByTitle(String title);

    List<WorkItem> listByStatus(int id);

    List<WorkItem> listByUser(int id);

    void create(WorkItem workItem);

    void update(WorkItem workItem);

    void delete(int id);

    List<WorkItem> filter(Optional<Integer> statusId,
                          Optional<Integer> reviewerId,
                          int creatorId,
                          Optional<String> sort);

    List<WorkItem> sortBy(Optional<String> sort);


}
