package com.teamwork.peerreview.repositories.contracts;


import com.teamwork.peerreview.models.Comment;

public interface CommentRepository {
    public void create(Comment comment);

    void delete(int id);

    Comment getById(int id);
}
