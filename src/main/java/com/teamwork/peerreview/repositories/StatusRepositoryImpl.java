package com.teamwork.peerreview.repositories;

import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.models.Status;
import com.teamwork.peerreview.repositories.contracts.StatusRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.management.Query;
import java.util.List;

@Repository
public class StatusRepositoryImpl implements StatusRepository {
    private final SessionFactory sessionFactory;


    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Status> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Status ", Status.class).list();
        }
    }

    @Override
    public Status getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            List<Status> result = session
                    .createQuery("from Status where name = :name", Status.class)
                    .setParameter("name", name)
                    .list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Team", "name", name);
            }
            return result.get(0);
        }

    }

    @Override
    public Status getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Status result = session.get(Status.class, id);
            if(result == null){
                throw new EntityNotFoundException("Team", id);
            }
            return result;
        }
    }
}

