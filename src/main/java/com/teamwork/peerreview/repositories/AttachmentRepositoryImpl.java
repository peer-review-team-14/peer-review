package com.teamwork.peerreview.repositories;

import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.models.Attachment;
import com.teamwork.peerreview.repositories.contracts.AttachmentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AttachmentRepositoryImpl implements AttachmentRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public AttachmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Attachment getById(int id) {
        try (
                Session session = sessionFactory.openSession()) {
            Attachment result = session.get(Attachment.class, id);
            if (result == null) {
                throw new EntityNotFoundException("Attachment", id);
            }
            return result;
        }
    }

    @Override
    public void create(Attachment attachment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(attachment);
            session.getTransaction().commit();
        }


    }

    @Override
    public void update(Attachment attachment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(attachment);
            session.getTransaction().commit();
        }
    }


    @Override
    public void delete(int id) {
        Attachment attachmentToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(attachmentToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Attachment> getByWorkItemId(int workItemId) {

        try (Session session = sessionFactory.openSession()) {
            List<Attachment> result = session.createQuery("from Attachment where workItem.id = :workitemId", Attachment.class)
                    .setParameter("workitemId", workItemId)
                    .list();
            if (result == null) {
                throw new EntityNotFoundException("WorkItem", workItemId);
            }
            return result;
        }
    }

}
