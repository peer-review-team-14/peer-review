package com.teamwork.peerreview.repositories;


import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.models.WorkItem;
import com.teamwork.peerreview.repositories.contracts.WorkItemRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class WorkItemRepositoryImpl implements WorkItemRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public WorkItemRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<WorkItem> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from WorkItem", WorkItem.class).list();
        }
    }

    @Override
    public WorkItem getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            WorkItem result = session.get(WorkItem.class, id);
            if (result == null) {
                throw new EntityNotFoundException("Work item", id);
            }
            return result;
        }
    }

    @Override
    public WorkItem getByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            List<WorkItem> result = session.createQuery("from WorkItem where title = :title", WorkItem.class)
                    .setParameter("title", title)
                    .list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Work item", "title", title);
            }
            return result.get(0);
        }
    }

    @Override
    public List<WorkItem> listByStatus(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from WorkItem where status.id = :id", WorkItem.class)
                    .setParameter("id", id)
                    .list();
        }
    }

    @Override
    public List<WorkItem> listByUser(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from WorkItem where userCreator.id = :id", WorkItem.class)
                    .setParameter("id", id)
                    .list();
        }
    }

    @Override
    public void create(WorkItem workItem) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(workItem);
            session.getTransaction().commit();
        }


    }

    @Override
    public void update(WorkItem workItem) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(workItem);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        WorkItem workItemToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(workItemToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<WorkItem> filter(Optional<Integer> statusId,
                                 Optional<Integer> reviewerId,
                                 int creatorId,
                                 Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder("from WorkItem ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            filters.add(" creator_Id = :creatorId ");
            params.put("creatorId", creatorId);

            if (statusId.get() != -1) {
                statusId.ifPresent(value -> {
                    filters.add(" status_Id = :statusId ");
                    params.put("statusId", value);
                });
            }
            if (reviewerId.get() != -1) {
                reviewerId.ifPresent(value -> {
                    filters.add(" reviewer_Id = :reviewerId ");
                    params.put("reviewerId", value);
                });

            }
            if (!filters.isEmpty()) {
                queryString.append(" where ")
                        .append(String.join(" and ", filters));
            }
            if (!sort.get().equals("asc")) {
                sort.ifPresent(value -> {
                    queryString.append(generateSortingString(value));
                });
            }
            System.out.println(queryString);
            return session.createQuery(queryString.toString(), WorkItem.class)
                    .setProperties(params)
                    .list();
        }
    }

    @Override
    public List<WorkItem> sortBy(Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder("from WorkItem ");
            sort.ifPresent(value -> queryString.append(generateSortingString(value)));
            System.out.println(queryString);
            return session.createQuery(queryString.toString(), WorkItem.class).list();
        }
    }

    private String generateSortingString(String value) {
        StringBuilder result = new StringBuilder(" order by ");

        switch (value) {
            case "Id, ascending":
                result.append("id asc");
                break;
            case "Id, descending":
                result.append("id desc");
                break;
            case "Title, ascending":
                result.append("title asc");
                break;
            case "Title, descending":
                result.append("title desc");
                break;
            case "Status, ascending":
                result.append("status asc");
                break;
            case "Status, descending":
                result.append("status desc");
                break;
        }

        return result.toString();
    }

}
