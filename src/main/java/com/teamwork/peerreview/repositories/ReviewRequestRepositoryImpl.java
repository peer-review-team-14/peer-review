package com.teamwork.peerreview.repositories;

import com.teamwork.peerreview.repositories.contracts.ReviewRequestRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReviewRequestRepositoryImpl implements ReviewRequestRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ReviewRequestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
