package com.teamwork.peerreview.repositories;

import com.teamwork.peerreview.exceptions.EntityNotFoundException;
import com.teamwork.peerreview.models.Team;
import com.teamwork.peerreview.models.User;
import com.teamwork.peerreview.repositories.contracts.TeamRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class TeamRepositoryImpl implements TeamRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public TeamRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Team> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Team ", Team.class).list();
        }
    }

    @Override
    public Team getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Team team = session.get(Team.class, id);
            if (team == null) {
                throw new EntityNotFoundException("Team", id);
            }
            return team;
        }
    }

    @Override
    public Team getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            List<Team> result = session.createQuery("from Team where name like :teamname", Team.class)
                    .setParameter("teamname", name)
                    .list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Team", "name", name);
            }
            return result.get(0);

        }
    }

    @Override
    public List<Team> getAllTeamWhereUserBelong(int userId) {
        try (Session session = sessionFactory.openSession()) {
            List<Team> teams = session.createNativeQuery("select teams.id, teams.name, teams.owner_id from teams\n" +
                            "join teams_members tm on teams.id = tm.team_id\n" +
                            "where user_id = :userId", Team.class)
                    .setParameter("userId", userId)
                    .list();
            if (teams.size() == 0) {
                throw new EntityNotFoundException("Teams with user", userId);
            }
            return teams;
        }
    }





    @Override
    public void create(Team team) {
        try (Session session = sessionFactory.openSession()) {
            var transaction = session.beginTransaction();
            session.save(team);
            transaction.commit();
        }
    }

    @Override
    public void update(Team team) {
        try (Session session = sessionFactory.openSession()) {
            var transaction = session.beginTransaction();
            session.update(team);
            transaction.commit();
        }
    }

    @Override
    public void delete(int id) {
        Team teamToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            var transaction = session.beginTransaction();
            session.delete(teamToDelete);
            transaction.commit();
        }
    }

    public void deleteMemberFromTeam(int id) {
        try (Session session = sessionFactory.openSession()) {
            var transaction = session.beginTransaction();
            session.createNativeQuery("delete from teams_members where user_id = :id")
                    .setParameter("id", id)
                    .executeUpdate();
            transaction.commit();
        }
    }
}
