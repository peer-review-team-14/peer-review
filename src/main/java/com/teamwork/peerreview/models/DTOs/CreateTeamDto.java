package com.teamwork.peerreview.models.DTOs;

import com.teamwork.peerreview.models.User;
import org.springframework.lang.NonNull;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

public class CreateTeamDto {

    @NotEmpty
    @Size(min = 3, max = 30, message = "Name should be between 3 and 30")
    private String name;

    private int teamOwnerId;

    private List<Integer> teamMembers;

    public CreateTeamDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTeamOwnerId() {
        return teamOwnerId;
    }

    public void setTeamOwnerId(int teamOwnerId) {
        this.teamOwnerId = teamOwnerId;
    }

    public List<Integer> getTeamMembers() {
        return teamMembers;
    }

    public void setTeamMembers(List<Integer> teamMembers) {
        this.teamMembers = teamMembers;
    }
}
