package com.teamwork.peerreview.models.DTOs;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class WorkItemEditDto {
    @NotEmpty
    @Size(min = 10, max = 80)
    private String title;

    @NotEmpty
    @Size(min = 20)
    private String description;





    public WorkItemEditDto() {
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
