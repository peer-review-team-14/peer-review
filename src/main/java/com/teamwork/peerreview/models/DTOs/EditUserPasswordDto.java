package com.teamwork.peerreview.models.DTOs;

import com.teamwork.peerreview.annotations.ValidPassword;

import javax.validation.constraints.NotEmpty;

public class EditUserPasswordDto {

    @NotEmpty
    @ValidPassword()
    private String password;

    @NotEmpty(message = "Password confirmation can't be empty")
    @ValidPassword()
    private String passwordConfirm;

    public EditUserPasswordDto() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
