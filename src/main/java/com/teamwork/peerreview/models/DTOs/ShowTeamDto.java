package com.teamwork.peerreview.models.DTOs;

public class ShowTeamDto {

    String name;
    ShowUserDto showTeamOwnerDto;

    public ShowTeamDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ShowUserDto getShowTeamOwnerDto() {
        return showTeamOwnerDto;
    }

    public void setShowTeamOwnerDto(ShowUserDto showTeamOwnerDto) {
        this.showTeamOwnerDto = showTeamOwnerDto;
    }
}
