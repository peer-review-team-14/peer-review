package com.teamwork.peerreview.models.DTOs;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


public class WorkItemUpdateDto {

    @NotEmpty
    @Size(min = 10, max = 80)
    private String title;

    @NotEmpty
    @Size(min = 20)
    private String description;

    String comments;

    private int statusId;

    private int userCreatorId;

    private int reviewerId;

    public WorkItemUpdateDto() {
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getUserCreatorId() {
        return userCreatorId;
    }

    public void setUserCreatorId(int userCreatorId) {
        this.userCreatorId = userCreatorId;
    }

    public int getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(int reviewerId) {
        this.reviewerId = reviewerId;
    }
}
