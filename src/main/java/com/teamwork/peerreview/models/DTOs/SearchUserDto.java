package com.teamwork.peerreview.models.DTOs;

public class SearchUserDto {

    private String search;

    public SearchUserDto() {

    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
