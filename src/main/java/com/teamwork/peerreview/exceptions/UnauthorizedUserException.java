package com.teamwork.peerreview.exceptions;

public class UnauthorizedUserException extends RuntimeException{
    public UnauthorizedUserException(String value) {
        super(value);
    }
}
