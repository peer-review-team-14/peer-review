-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.4-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table peer_review_db.attachments: ~0 rows (approximately)
/*!40000 ALTER TABLE `attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachments` ENABLE KEYS */;

-- Dumping data for table peer_review_db.comments: ~0 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping data for table peer_review_db.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`) VALUES
	(1, 'admin'),
	(2, 'user');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table peer_review_db.statuses: ~5 rows (approximately)
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`id`, `name`) VALUES
	(6, 'Accepted'),
	(5, 'Change Requested'),
	(3, 'Pending'),
	(7, 'Rejected'),
	(4, 'Under Review');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

-- Dumping data for table peer_review_db.teams: ~6 rows (approximately)
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` (`id`, `name`, `owner_id`) VALUES
	(1, 'lavina', 1),
	(2, 'Product Pushers', 2),
	(3, 'New Millennium', 17),
	(4, 'Justice League', 18),
	(5, 'Hellraisers', 19),
	(6, 'The Foundation', 20);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;

-- Dumping data for table peer_review_db.teams_members: ~26 rows (approximately)
/*!40000 ALTER TABLE `teams_members` DISABLE KEYS */;
INSERT INTO `teams_members` (`id`, `team_id`, `user_id`) VALUES
	(8, 2, 2),
	(9, 1, 1),
	(10, 2, 9),
	(11, 1, 9),
	(13, 6, 1),
	(14, 2, 26),
	(15, 1, 27),
	(16, 6, 28),
	(17, 3, 29),
	(18, 3, 30),
	(19, 3, 31),
	(20, 6, 32),
	(21, 4, 33),
	(22, 4, 34),
	(23, 4, 35),
	(24, 6, 36),
	(25, 5, 37),
	(26, 5, 38),
	(27, 5, 39),
	(28, 3, 17),
	(29, 4, 18),
	(30, 5, 19),
	(31, 6, 20),
	(32, 4, 2),
	(34, 3, 2),
	(35, 2, 1);
/*!40000 ALTER TABLE `teams_members` ENABLE KEYS */;

-- Dumping data for table peer_review_db.users: ~27 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `email`, `phone_number`, `photo`, `role_id`) VALUES
	(1, 'nikola', 'Nikola1!', 'nikola@telerikacademy.com', '0882123456', 'image00002.jpg', 2),
	(2, 'lubo', 'Lubomir1!', 'lubo@telerikacademy.com', '0883123456', NULL, 2),
	(9, 'gabi', 'Gabriela1!', 'gabi@telerikacademy.com', '0884123456', NULL, 2),
	(10, 'admin', 'admin', 'admin@telericacademy.com', '0885123456', NULL, 1),
	(17, 'todd', 'Lubomir1!', 'todd@telerikacademy.com', '0886123456', NULL, 2),
	(18, 'ivan', 'Nikola1!', 'ivan@telerikacademy.com', '0887123456', NULL, 2),
	(19, 'radin', 'Lubomir1!', 'radin@telerikacademy.com', '0888123456', NULL, 2),
	(20, 'dimitar', 'Nikola1!', 'dimitar@telerikacademy.com', '0889123456', NULL, 2),
	(21, 'dimi', 'Lubomir1!', 'dimi@telerikacademy.com', '0889223456', NULL, 2),
	(22, 'georgi', 'Nikola1!', 'georgi@telerikacademy.com', '0889228456', NULL, 2),
	(23, 'nikolay', 'Lubomir1!', 'nikolay@telerikacademy.com', '0889223457', NULL, 2),
	(24, 'kaloyan', 'Nikola1!', 'kaloyan@telerikacademy.com', '0889223458', NULL, 2),
	(25, 'plamen', 'Lubomir1!', 'plamen@telerikacademy.com', '0889223459', NULL, 2),
	(26, 'hristo', 'Nikola1!', 'hristo@telerikacademy.com', '0889223460', NULL, 2),
	(27, 'simeon', 'Lubomir1!', 'simeon@telerikacademy.com', '0889223461', NULL, 2),
	(28, 'valeri', 'Nikola1!', 'valeri@telerikacademy.com', '0889223462', NULL, 2),
	(29, 'genadi', 'Lubomir1!', 'genadi@telerikacademy.com', '0889223463', NULL, 2),
	(30, 'maria', 'Nikola1!', 'maria@telerikacademy.com', '0889223464', NULL, 2),
	(31, 'ralica', 'Lubomir1!', 'ralica@telerikacademy.com', '0889223465', NULL, 2),
	(32, 'elena', 'Nikola1!', 'elena@telerikacademy.com', '0889223466', NULL, 2),
	(33, 'milena', 'Lubomir1!', 'milena@telerikacademy.com', '0889223467', NULL, 2),
	(34, 'monika', 'Nikola1!', 'monika@telerikacademy.com', '0889223468', NULL, 2),
	(35, 'simona', 'Lubomir1!', 'simona@telerikacademy.com', '0889223469', NULL, 2),
	(36, 'kristina', 'Nikola1!', 'kristina@telerikacademy.com', '0889223470', NULL, 2),
	(37, 'hristina', 'Lubomir1!', 'hristina@telerikacademy.com', '0889223471', NULL, 2),
	(38, 'nadejda', 'Nikola1!', 'nadejda@telerikacademy.com', '0889223472', NULL, 2),
	(39, 'lubov', 'Lubomir1!', 'lubov@telerikacademy.com', '0889223473', NULL, 2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table peer_review_db.work_items: ~17 rows (approximately)
/*!40000 ALTER TABLE `work_items` DISABLE KEYS */;
INSERT INTO `work_items` (`id`, `title`, `description`, `status_id`, `creator_id`, `reviewer_id`, `team_id`) VALUES
	(11, 'Designing application ', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 4, 2, 18, 4),
	(12, 'Implementing application', 'Lorem ipsum dolor sit amet', 3, 2, 9, 2),
	(13, 'Maintaining application', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. \n\n', 6, 2, 33, 4),
	(14, 'Monitor production', 'raesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', 4, 2, 26, 2),
	(15, 'Performance of the application', 'Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor.', 3, 2, 34, 4),
	(16, 'Security of the application', 'Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum.', 6, 2, 9, 2),
	(17, 'Quality of the application', 'ulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh. ', 4, 2, 35, 4),
	(18, 'Implement innovative solutions ', 'Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ', 3, 2, 26, 2),
	(19, 'New features and products', 'Nam nec ante. Sed lacinia, urna non tincidunt mattis, tortor neque adipiscing diam, a cursus ipsum ante quis turpis. Nulla facilisi. Ut fringilla. ', 6, 2, 17, 3),
	(20, 'Exception handling', 'Suspendisse potenti. Nunc feugiat mi a tellus consequat imperdiet. Vestibulum sapien.', 4, 2, 9, 2),
	(21, 'Developing documentation', 'Proin quam. Etiam ultrices. Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod lacus luctus magna. Quisque cursus, metus vitae pharetra auctor, sem massa mattis sem, at interdum magna augue eget diam. ', 3, 2, 31, 3),
	(22, 'Production problems', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi lacinia molestie dui. Praesent blandit dolor.', 6, 2, 26, 2),
	(23, 'Establishing network systems', 'Sed non quam. In vel mi sit amet augue congue elementum. Morbi in ipsum sit amet pede facilisis laoreet. Donec lacus nunc, viverra nec.', 4, 2, 30, 3),
	(24, 'Establishing search engine', 'Quisque cursus, metus vitae pharetra auctor, sem massa mattis sem, at interdum magna augue eget diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi lacini.', 3, 2, 9, 2),
	(25, 'Designing and conducting tests', 'Suspendisse potenti. Nunc feugiat mi a tellus consequat imperdiet. Vestibulum sapien. Proin quam. Etiam ultrices. Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod lacus luctus magna. ', 6, 2, 29, 3),
	(26, 'Design and develop UI  ', ' Aenean quam. In scelerisque sem at dolor. Maecenas mattis.', 4, 2, 26, 2),
	(27, 'Data analysis ', 'Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh. ', 3, 2, 17, 3);
/*!40000 ALTER TABLE `work_items` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
